#pragma once

#include <cstddef>
#include <cmath>
#include <stdexcept>

class NegBinProbRange;

class NegBinDist {
    double p_;
    size_t k_;

  public:
    template <typename T>
    NegBinDist(double p, T k) : p_(p), k_(k) 
    {
        static_assert(std::is_integral<T>::value, "T must be integral type");
        if (p <= 0.0 || p > 1.0)
            throw std::domain_error("p must be within interval (0, 1)");
        if (k == 0)
            throw std::domain_error("k must be positive");
    }

    double p() const { return p_; }
    size_t k() const { return k_; }

    typedef NegBinProbRange ProbRange;
};

class NegBinProbRange {
    const NegBinDist& dist_;
    double current_p_;
    unsigned j_;
    double q_;
  public:
    NegBinProbRange(const NegBinDist& dist)
        : dist_(dist), current_p_(std::pow(dist.p(), dist.k())), 
          j_(0), q_(1 - dist.p()) {}

    bool empty() const { return false; }
    double front() const { return current_p_; }
    void popFront() { 
        current_p_ *= q_;
        current_p_ *= dist_.k() + j_;
        current_p_ /= j_ + 1;
        ++j_;
    }
};
