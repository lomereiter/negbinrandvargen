#pragma once

#include "ext/root-mathcore/SpecFuncCephes.h"

class ChiSquareDist {
    unsigned df_;

  public:
    ChiSquareDist(unsigned df) : df_(df) {}

    double cdf(double x) const {
		return ROOT::Math::Cephes::igam(0.5 * df_, 0.5 * x);
    }

	double cdf_c(double x) const {
		return ROOT::Math::Cephes::igamc(0.5 * df_, 0.5 * x);
	}
};
