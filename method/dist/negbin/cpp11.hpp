#pragma once

#include "method/iface.hpp"
#include "dist/negbin.hpp"

#include <random>

#include <ctime>

class NegBinCpp11Gen : public RandVarGen<unsigned> {

    std::negative_binomial_distribution<unsigned> dist_;
    std::mt19937 rng_;

  public:

    NegBinCpp11Gen(const NegBinDist& dist) : dist_(dist.k(), dist.p()) {
        rng_.seed(time(NULL));
    }

    virtual unsigned generate() {
        return dist_(rng_);
    }
};
