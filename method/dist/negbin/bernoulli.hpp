#pragma once

#include "method/iface.hpp"
#include "dist/negbin.hpp"

#include <random>
#include <ctime>

class NegBinBernoulliGen : public RandVarGen<unsigned> {

    const NegBinDist& dist_;
    std::uniform_real_distribution<double> unif_;
    std::mt19937 rng_;

  public:

    NegBinBernoulliGen(const NegBinDist& dist) : dist_(dist), unif_(0.0, 1.0) {
        rng_.seed(time(NULL));
    }

    virtual unsigned generate() {
        auto p = dist_.p();
        auto k = dist_.k();

        unsigned fails = 0, successes = 0;
        while (true) {
            if (unif_(rng_) <= p)
            {
                if (++fails == k)
                    return successes;
            } else {
                ++successes;
            }
        }
    }
};
