#pragma once

#include "method/iface.hpp"
#include "dist/negbin.hpp"

#include <random>
#include <algorithm>
#include <cmath>
#include <ctime>

class NegBinTableGen : public RandVarGen<unsigned> {

    const NegBinDist& dist_;
    std::uniform_real_distribution<double> unif_;
    std::mt19937 rng_;
    std::vector<double> s_;

  public:

    NegBinTableGen(const NegBinDist& dist, double eps=1e-12)
        : dist_(dist), unif_(0.0, 1.0)
    {
        rng_.seed(time(NULL));

        auto p = dist_.p();
        auto k = dist_.k();

        double q = 1.0 - p;
        double l = std::pow(p, static_cast<double>(k));
        s_.push_back(l);

        for (size_t j = 0; std::fabs(s_.back() - 1.0) > eps; ++j) {
            // p_0 = p^k
            // p_j = C_{k+j-1}^{j} * p^k * q^j = 
            //     = (k + j - 1)! / j! / (k - 1)! * p^k * q^j
            // p_{j+1} = p_j * (k + j) * q / (j + 1)
            l *= q * (k + j);
            l /= j + 1;
            s_.push_back(s_.back() + l);
        }
    }

    virtual unsigned generate() {
        double alpha = unif_(rng_);
        return std::upper_bound(s_.begin(), s_.end(), alpha) - s_.begin();
    }
};
