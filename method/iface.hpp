#pragma once

#include <vector>
#include <cstddef>

template <typename RandVarType>
class RandVarGen {

  public:
    std::vector<RandVarType> sample(size_t n) {
        std::vector<RandVarType> result(n);
        fill(result.begin(), result.end());
        return result;
    }

    template <typename It>
    void fill(It from, It to) {
        for (auto it = from; it != to; ++it)
            *it = generate();
    }

    virtual RandVarType generate() = 0;
    virtual ~RandVarGen() {}
};
