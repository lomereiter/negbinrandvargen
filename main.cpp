#include "method/iface.hpp"

#include "dist/negbin.hpp"
#include "stat_tests/chi_square.hpp"
#include "method/dist/negbin/bernoulli.hpp"
#include "method/dist/negbin/table.hpp"
#include "method/dist/negbin/cpp11.hpp"

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <memory>
#include <numeric>
#include <vector>
#include <string>
#include <sstream>
#include <array>

const int DELIMITER_LINE_LENGTH = 140;

void print_delimiter_line() {
    for (auto i = 0; i < DELIMITER_LINE_LENGTH; ++i)
        std::cout << "=";
    std::cout << std::endl;
}

class ProgressBar {
    unsigned width_;
  public:
    ProgressBar(unsigned width) : width_(width) {
        std::cout << ">";
    }

    void update(double percentage) {
        std::cout << "\r";
        auto filled = width_ * percentage;
        for (auto i = 0; i < filled; ++i)
            std::cout << "=";
        std::cout << ">";
    }

    void finish() {
        std::cout << "\rDone.";
        for (auto i = 0; i < width_ - 4; ++i)
            std::cout << " ";
        std::cout << std::endl;
    }
};

template <typename T>
void print_levels(const T& dist, bool include_zero=true) {
    if (include_zero)
        std::cout << "     0 ";
    auto bin_size = 1.0 / (dist.size() - 1);
    for (auto i = 0; i < dist.size() - 1; ++i)
        std::cout << std::setw(6) << std::setprecision(3) << (i + 1) * bin_size << " ";
    std::cout << std::endl;
}

template <typename T>
void print_distribution(const T& dist) {
    print_levels(dist);

    std::cout << "   ";
    for (auto i = 0; i < dist.size() - 1; ++i)
        std::cout << std::setw(6) << dist[i] << " ";

    std::cout << std::endl;
}

template <typename T>
void print_stat_power(const T& dist) {
    print_levels(dist, false);

    unsigned total = std::accumulate(dist.begin(), dist.end(), 0);
    unsigned cumsum = 0;

    for (auto i = 0; i < dist.size() - 1; ++i) {
        cumsum += dist[i];
        auto power = cumsum * 1.0 / total;
        std::cout << std::setw(6) << std::setprecision(3) << power << " ";
    }

    std::cout << std::endl;
}


template <typename T>
bool readValue(T &param) {
    T val;
    std::string line;
    std::getline(std::cin, line);
    std::stringstream ss(line);
    if (ss >> val) {
        if (ss.eof()) {
            param = val;
            return true;
        }
    }
    return false;
}

void get_distribution_params(double* p, unsigned* n) {
    double p_;
    int n_;
set_p:
    std::cout << "    p = ";
    if (!readValue(p_) || p_ < 0 || p_ > 1) {
        std::cerr << "[error] enter number from range 0 .. 1" << std::endl;
        goto set_p;
    }
set_n:
    std::cout << "    n = ";
    if (!readValue(n_) || n_ < 1) {
        std::cerr << "[error] enter positive number" << std::endl;
        goto set_n;
    }
    *p = p_;
    *n = n_;
}

int main() {

    double p1, p2;
    unsigned n1, n2;
    int sample_size;
    int p_v_sample_size;

    std::cout << "Enter H0 negative binomial distribution parameters" << std::endl;
    get_distribution_params(&p1, &n1);
    NegBinDist h0(p1, n1);
    std::unique_ptr<RandVarGen<unsigned>> gen = NULL;
    unsigned method;

select_method:
    std::cout << "Select method of random variable generation:" << std::endl;
    std::cout << "    1 - via Bernoulli trials" << std::endl;
    std::cout << "    2 - table inverse function method (uses binary search)" << std::endl;
    std::cout << "    3 - built-in C++11 std::negative_binomial_distribution" << std::endl;
    if (!readValue(method)) {
        std::cerr << "[error] invalid input" << std::endl;
        goto select_method;
    }

    RandVarGen<unsigned>* gen_ptr;
    switch (method) {
        case 1:
            gen_ptr = new NegBinBernoulliGen(h0);
            break;
        case 2:
            gen_ptr = new NegBinTableGen(h0);
            break;
        case 3:
            gen_ptr = new NegBinCpp11Gen(h0);
            break;
        default:
            std::cerr << "[error] invalid input" << std::endl;
            goto select_method;
    }
    gen = std::unique_ptr<RandVarGen<unsigned>>(gen_ptr);

    print_delimiter_line();
    std::cout << "Enter H1 negative binomial distribution parameters" << std::endl;
    get_distribution_params(&p2, &n2);

    NegBinDist h1(p2, n2);

    print_delimiter_line();
set_sample_size:
    std::cout << "Enter negative binomial distribution sample size: ";
    if (!readValue(sample_size) || sample_size < 1) {
        std::cerr << "[error] sample size must be positive" << std::endl;
        goto set_sample_size;
    }
    print_delimiter_line();
set_pv_sample_size:
    std::cout << "Enter p-value sample size: ";
    if (!readValue(p_v_sample_size) || p_v_sample_size < 1) {
        std::cerr << "[error] sample size must be positive" << std::endl;
        goto set_pv_sample_size;
    }

    print_delimiter_line();
    std::cout << "Generating data..." << std::endl;

    ProgressBar progressbar(DELIMITER_LINE_LENGTH);

    const unsigned N = 20;
    auto p_level_dist0 = std::array<unsigned, N + 1>();
    auto p_level_dist1 = std::array<unsigned, N + 1>();

    std::vector<unsigned> sample_buf(sample_size);
    for (auto i = 0; i < p_v_sample_size; ++i) {
        gen->fill(sample_buf.begin(), sample_buf.end());
        auto p_level0 = pLevel(sample_buf, h0);
        ++p_level_dist0[static_cast<size_t>(std::floor(p_level0 * N))];

        auto p_level1 = pLevel(sample_buf, h1);
        ++p_level_dist1[static_cast<size_t>(std::floor(p_level1 * N))];

        if (i & 0x8FF == 0x8FF)
            progressbar.update(i * 1.0 / p_v_sample_size);
    }

    progressbar.finish();

    print_delimiter_line();
    std::cout << "p-value distribution for H0" << std::endl;
    print_distribution(p_level_dist0);

    print_delimiter_line();
    std::cout << "p-value distribution for H1" << std::endl;
    print_distribution(p_level_dist1);

    print_delimiter_line();
    std::cout << "Statistical power against H1" << std::endl;
    print_stat_power(p_level_dist1);
}
