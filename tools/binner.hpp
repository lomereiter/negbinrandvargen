#pragma once

#include <vector>
#include <numeric>
#include <cassert>

class Bin {
    unsigned observed_;
    double expected_;
  public:
    Bin() : observed_(0), expected_(0.0) {}
    Bin(unsigned observed, double expected)
        : observed_(observed), expected_(expected) {}
    unsigned observed() const { return observed_; }
    double expected() const { return expected_; }
};

template <typename Dist>
class Binner {
    const std::vector<unsigned>& counts_;
    unsigned n_;

    typename Dist::ProbRange probs_;
    unsigned j_;
    double p_j_;
    double tail_;

    unsigned min_denom_;

    bool empty_;
    Bin front_;
  public:
    Binner(const std::vector<unsigned>& counts, 
           const Dist& dist, 
           unsigned min_denominator=5)
        : counts_(counts),
          n_(std::accumulate(counts.begin(), counts.end(), 0)),
          probs_(typename Dist::ProbRange(dist)), 
          j_(0), p_j_(0.0), tail_(1.0), min_denom_(min_denominator),
          empty_(false)
    {
        getNextBin();
    }

    bool empty() const { return empty_; }

    Bin front() const { return front_; }

    void popFront() {
        probs_.popFront();
        getNextBin();
    }

  private:
    void getNextBin() {
        if (j_ >= counts_.size()) {
            empty_ = true;
            return;
        }

        p_j_ = probs_.front();
        double expected = n_ * p_j_;
        tail_ -= p_j_;
        unsigned observed = counts_[j_++];

        while (expected <= min_denom_ && j_ < counts_.size() && !probs_.empty()) {
            probs_.popFront();
            p_j_ = probs_.front();
            expected += n_ * p_j_;
            tail_ -= p_j_;
            observed += counts_[j_++];
        }

        if (n_ * tail_ < min_denom_) {
            observed += std::accumulate(counts_.begin() + j_, counts_.end(), 0);
            expected += n_ * tail_;
            j_ = counts_.size();
            tail_ = 0.0;
        }

        front_ = Bin(observed, expected);
    }
};
