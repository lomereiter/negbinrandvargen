all: cephes.o main.o
	g++ main.o cephes.o -o main

main.o: main.cpp
	g++ -c -O2 -std=c++0x -O2 -I. main.cpp -o main.o

cephes.o: ext/root-mathcore/SpecFuncCephes.cxx
	g++ -c -O2 -Iext/root-mathcore ext/root-mathcore/SpecFuncCephes.cxx -o cephes.o

clean:
	rm -f main cephes.o main.o
