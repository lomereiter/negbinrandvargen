#pragma once

#include "dist/chi_square.hpp"
#include "tools/binner.hpp"

#include <vector>
#include <cmath>
#include <cstddef>
#include <algorithm>

/// Result is a pair of the chi-square statistic and #d.f.
template <typename Dist>
std::pair<unsigned, double> 
chiSquareStatistic(const std::vector<unsigned>& sample, const Dist& dist) {

    if (sample.empty())
        return std::make_pair(0U, 0.0);

    auto max_element = std::max_element(sample.cbegin(), sample.cend());
    std::vector<unsigned> counts(*max_element + 1);
    
    for (auto it = sample.cbegin(); it != sample.cend(); ++it) {
        ++counts[*it];
    }

    double chi_sq = 0.0;
    unsigned n_bins = 0;

    Binner<Dist> binner(counts, dist, 5);

    while (!binner.empty()) {
        auto bin = binner.front();

        double diff = bin.observed() - bin.expected();
        chi_sq += diff * diff / bin.expected();
        ++n_bins;

        binner.popFront();
    }

    return std::make_pair(n_bins - 1, chi_sq);
}

/// P-level for the corresponding chi-square statistic.
template <typename Dist>
double pLevel(const std::vector<unsigned>& sample, const Dist& dist) {
    auto chi_sq = chiSquareStatistic(sample, dist);
    
    auto df = chi_sq.first;
    auto statistic = chi_sq.second;

    return 1.0 - ChiSquareDist(df).cdf(statistic);
}
